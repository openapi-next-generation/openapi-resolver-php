<?php

namespace OpenapiNextGeneration\OpenapiResolverPhp;

use OpenapiNextGeneration\GenerationHelperPhp\SpecificationModelsHelper;

class DeepStructureOptimizer
{
    protected $refBasePath;
    protected $models;


    public function optimizeModels(array $specification): array
    {
        $this->refBasePath = '#/' . implode('/', SpecificationModelsHelper::fetchModelsPath($specification)) . '/';
        $this->models = SpecificationModelsHelper::readModels($specification);

        foreach (array_keys($this->models) as $modelName) {
            $this->moveChildContainersIntoNewModel($modelName);
        }

        return SpecificationModelsHelper::writeModels($specification, $this->models);
    }

    protected function moveChildContainersIntoNewModel(string $modelName): void
    {
        $model = &$this->models[$modelName];
        switch ($model['type'] ?? 'object') {
            case 'object':
                foreach ($model['properties'] as $propertyName => &$property) {
                    $propertyType = $property['type'] ?? 'object';
                    if ($propertyType === 'object' && !isset($property['$ref'])) {
                        $newModelName = $this->createModelName($modelName, $propertyName);
                        $this->models[$newModelName] = $property;
                        $property = ['$ref' => $this->refBasePath . $newModelName];
                        $this->moveChildContainersIntoNewModel($newModelName);
                    } elseif (
                        $propertyType === 'array' && !isset($property['$ref'])
                        && (
                            ($property['items']['type'] ?? 'object') === 'object'
                            || ($property['items']['type'] ?? 'object') === 'array'
                        )
                    ) {
                        $newModelName = $this->createModelName($modelName, $propertyName, true);
                        $this->models[$newModelName] = $property['items'];
                        $property['items'] = ['$ref' => $this->refBasePath . $newModelName];
                        $this->moveChildContainersIntoNewModel($newModelName);
                    }
                }
                break;
            case 'array':
                $itemType = $model['items']['type'] ?? 'object';
                if (
                    ($itemType === 'object' || $itemType === 'array')
                    && !isset($model['items']['$ref'])
                ) {
                    $newModelName = $this->createModelName($modelName, $propertyName, true);
                    $this->models[$newModelName] = $model['items'];
                    $model['items'] = ['$ref' => $this->refBasePath . $newModelName];
                    $this->moveChildContainersIntoNewModel($newModelName);
                }
                break;
            default:
                break;
        }
    }

    protected function createModelName(string $parentModelName, string $propertyName, bool $isItem = false): string
    {
        $modelName = ucfirst($propertyName);
        if ($isItem) {
            $modelName .= 'Item';
        }

        if (isset($this->models[$modelName])) {
            $modelName = ucfirst($parentModelName) . $modelName;
        }

        $checkModelName = $modelName;
        $suffix = 2;
        while (isset($this->models[$checkModelName])) {
            $checkModelName = $modelName . $suffix++;
        }
        return $checkModelName;
    }
}