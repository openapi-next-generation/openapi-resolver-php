<?php

namespace OpenapiNextGeneration\OpenapiResolverPhp;

class AllOfResolver
{
    /**
     * Resolve all 'allOf' occurrences and merge them
     */
    public function resolveKeywordAllOf(array $specification): array
    {
        foreach ($specification as $key => $subSpecification) {
            if ($key === 'allOf') {
                $mergeResult = [];
                foreach ($subSpecification as $mergeComponent) {
                    $mergeComponent = $this->resolveKeywordAllOf($mergeComponent);
                    $mergeResult = array_replace_recursive($mergeResult, $mergeComponent);
                }
                unset($specification[$key]);
                $specification = array_replace_recursive($specification, $mergeResult);
                if (isset($specification['$ref'])) {
                    unset($specification['$ref']);
                }
            } elseif (is_array($specification[$key])) {
                $specification[$key] = $this->resolveKeywordAllOf($subSpecification);
            }
        }
        return $specification;
    }
}